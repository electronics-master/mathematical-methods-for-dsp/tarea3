function x_k = k_truncate(x, K)
  abs_x = abs(x);
  x_k = [];

  K = sort(K)';

  for i=1:size(K)
    x_k = horzcat(x_k, x(:, K(i)) );
  endfor

endfunction
