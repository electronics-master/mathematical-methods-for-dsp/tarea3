function x = subspace_pursuit(phi, y, K)


  T = k_largest(ctranspose(phi)*y, K);

  phi_T = k_truncate(phi, T);

  y_r = resid(y, phi_T);
  y_old = y_r;

  while 1
    T_old = T;
    T = union(T, k_largest(ctranspose(phi) * y_r, K) );

    phi_T = k_truncate(phi, T);
    
    x_p = pinv(phi_T) * y;
    
    T = k_largest(x_p, K);

    y_old = y_r;
    y_r = resid(y, phi_T);

    if norm(y_r) >= norm(y_old)
      T = T_old;
      break;
    endif
  endwhile

  x = pinv(k_columns(phi, T)) * y;

endfunction
