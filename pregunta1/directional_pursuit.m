function [x_T, r_i] = directional_pursuit(A, y, k)
  ## Entradas:
  ##   A: Matriz de medición
  ##   y: Vector ruidoso de entrada
  ##   k: Esparcidad
  ## Parametros Iniciales:
  ##   r_0: Error recidual
  ##   x_0: Estimado inicial
  ##   T: Conjunto de columnas para conformar la esparcidad de x
  ## Salidas:
  ##   r: Error recidual
  ##   x: Vector estimado

  r_i = y;

  m = size(A)(2);

  x_T = zeros(m, 1);

  T = [];

  for i=1:k
    g_i = A' * r_i;

    j = 0;
    max_j = -Inf;
    for l=1:m
      arg = abs(g_i(l)) / norm(A(:, l));
      if arg > max_j
	max_j = arg;
	j = l;
      endif
    endfor
    
    T = union(T, [j]);

    A_T = [];
    for t=1:size(A)(2)
      if sum(T == t) == 1
	A_T = horzcat(A_T, A(:, t));
      else
	A_T = horzcat(A_T, zeros(size(A(:, t))));
      endif
    endfor

    d_T = A_T' * (y - A_T * x_T);

    c_i = A_T * d_T;
    a_i = dot(r_i, c_i) / norm(c_i) ** 2;

    x_T = x_T + a_i * d_T;
    r_i = r_i - a_i * c_i;

  endfor

endfunction
