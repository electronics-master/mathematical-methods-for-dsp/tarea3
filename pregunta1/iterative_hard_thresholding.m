function x_i = iterative_hard_thresholding(A, y, k, mu) 
  ## Entradas:
  ##   A: Matriz de medición
  ##   y: Vector ruidoso de entrada
  ##   k: Esparcidad
  ##   mu: Tamaño deel paso
  ## Parametros Iniciales:
  ##   x_0: Estimado inicial
  ## Salidas:
  ##   x: Vector estimado

  m = size(A)(2);

  x_i = zeros(m, 1) + 1e-4;
  x_old = zeros(m, 1);

  while (norm(x_i - x_old) / norm(x_i)) > 1e-3
    x_old = x_i;
    x_i = H_k(x_i + mu * A' * (y - A * x_i), k);
    i = i + 1;
  endwhile

endfunction
