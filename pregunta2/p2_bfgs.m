n = 5;

max_val = 500;
x = rand(n, 1) * 2 * max_val - max_val;
B = eye(n);

sigma_1 = 1e-4;
sigma_2 = 0.49;

alpha = 2;
epsilon = 1;

TOL = 1e-5;

i = 0;
error = [];

while 1
  i++;
  p = -inv(B) * g(x);
  
  lambda = 1;
  alpha = 0;
  beta = Inf;
  ## Implementing a bisect method
  while 1
    if f(x + lambda * p) - f(x) > sigma_1 * lambda * g(x)' * p
      beta = lambda;
      lambda = 0.5 * (alpha + beta);
    elseif g(x + lambda * p)' * p   < sigma_2 * g(x)' * p
      alpha = lambda;
      if beta == Inf
	lambda = 10 * alpha;
      else
	lambda = 0.5 * (alpha + beta);
      endif
    else
      break;
    endif
  endwhile

  old_x = x;
  x = x + lambda * p;

  s = x - old_x;
  y = g(x) - g(old_x);
  
  if (
      ((y' * s) / (norm(s) ** 2)) >
      epsilon * norm(g(x)) ** alpha
    )
    B = B - (B * s * s' * B) / (s' * B * s) + (y * y') / (y' * s);
  endif

  if norm(g(x)) <= TOL
    break;
  endif

  error = [error, norm(g(x))];
endwhile

x
min = f(x)

h = figure(1);
semilogy(error);
ylabel("Error");
xlabel("Iteraciones");
title("Iteraciones vs Error");
drawnow;

input("Press any key to exit");


print (h, "pregunta2", "-dpdflatex");

