function y_r = resid(y, phi)

  y_p = phi * pinv(phi) * y;
  y_r = y - y_p;
endfunction
