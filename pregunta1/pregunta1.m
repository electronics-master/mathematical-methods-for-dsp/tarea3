
n = 2000;
k = 8;

## Cargar el vector C
load("matriz_C.mat");

## Cargar el vector y con ruido
load("vector_con_ruido_y.mat");

## Cargar el vector de preimagenes
load("vector_t.mat");

## Cargar la matriz B, sino calcularla
if exist ("matriz_B.mat")
  load("matriz_B.mat");
  if size(B) != [n, n]
    B = dctmtx(n);
    save -binary "matriz_B.mat" B;
  endif
else
  B = dctmtx(n);
    save -binary "matriz_B.mat" B;
endif

## Cargar la matriz A, sino calcularla
if exist ("matriz_A.mat")
  load("matriz_A.mat");
  if size(A) != [size(y)(1), n]
    A = C * B';
    save -binary "matriz_A.mat" A;
  endif
else
  A = C * B';
  save -binary "matriz_A.mat" A;
endif

mu = 1e-4;

c_1 = matching_pursuit(A, y, k);
x_1 = B' * c_1;

c_2 = orthogonal_matching_pursuit(A, y, k);
x_2 = B' * c_2;

c_3 = directional_pursuit(A, y, k);
x_3 = B' * c_3;

c_4 = iterative_hard_thresholding(A, y, k, mu);
x_4 = B' * c_4;

c_5 = subspace_pursuit(A, y, k);
x_5 = B' * c_5;


h = figure(1);
subplot(3, 2, 1);
plot(t, x_1);
title("Matching Pursuit");

subplot(3, 2, 2);
plot(t, x_2);
title("Orthogonal Matching Pursuit");

subplot(3, 2, 3);
plot(t, x_3);
title("Directional Pursuit");

subplot(3, 2, 4);
plot(t, x_4);
title("Iterative Hard Thresholding");

subplot(3, 2, 5);
plot(t, x_5);
title("Subspace pursuit");

drawnow;

input("Press any key to exit");

