function K = k_largest(x, k)
  
  abs_x = abs(x);
  x_k = zeros(size(x));

  K = [];

  for i=1:k
    [value, index] = max(abs_x);
    K = horzcat(K, index);
    abs_x([index]) = 0;
  endfor

endfunction
