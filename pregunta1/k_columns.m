function x_k = k_columns(x, K)
  abs_x = abs(x);
  x_k = zeros(size(x));
  for i=1:K
    [value, index] = max(abs_x);
    x_k(:, index) = x(:, index);
    abs_x([index]) = 0;
  endfor
  
endfunction
