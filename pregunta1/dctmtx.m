function B = dctmtx(n)
  B = zeros(n, n);

  for r=1:n
    for s=1:n
      if r==1
	B(r, s) = 1 / sqrt(n);
      else

	B(r, s) = sqrt(2 / n) * cos ( (2 * s - 1) * (r - 1) * pi / (2 * n) ) ;
	
      endif
    endfor
  endfor
endfunction
