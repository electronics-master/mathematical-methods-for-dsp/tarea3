function x_k = H_k(x, k)
  abs_x = abs(x);
  x_k = zeros(size(x));
  for i=1:k
    [value, index] = max(abs_x);
    x_k(index) = x(index);
    abs_x([index]) = 0;
  endfor
 
endfunction
