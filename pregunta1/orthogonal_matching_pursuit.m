function [x, r] = orthogonal_matching_pursuit(A, y, k)
  ## Entradas:
  ##   A: Matriz de medición
  ##   y: Vector ruidoso de entrada
  ##   k: Esparcidad
  ## Parametros Iniciales:
  ##   r_0: Error recidual
  ##   x_0: Estimado inicial
  ##   T: Conjunto de columnas para conformar la esparcidad de x
  ## Salidas:
  ##   r: Error recidual
  ##   x: Vector estimado

  r = y;

  m = size(A)(2);

  x = zeros(1, m);

  T = [];

  for i=1:k
    g_i = A' * r;

    j = 0;
    max_j = -Inf;
    for l=1:m
      arg = abs(g_i(l)) / norm(A(:, l));
      if arg > max_j
	max_j = arg;
	j = l;
      endif
    endfor

    T = union(T, [j]);

    A_T = [];
    for t=1:size(A)(2)
      if sum(T == t) == 1
	A_T = horzcat(A_T, A(:, t));
      else
	A_T = horzcat(A_T, zeros(size(A(:, t))));
      endif
    endfor

    x = pinv(A_T) * y;
    r = y  - A * x;
  endfor

endfunction
